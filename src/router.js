import Vue from "vue";
import Router from "vue-router";
import Helper from "./helper/Helper";
import store from "./store/store";

Vue.use(Router);

function storageValidation() {
  try {
    return !!Helper.Decrypt(
      localStorage.getItem(process.env.VUE_APP_STORAGE_NAME)
    );
  } catch (error) {
    return false;
  }
}

function guardedAuth(to, from, next) {
  const storage = storageValidation();
  if ((!store.getters["auth/isLoggedIn"] || !storage) && to.path !== "/login") {
    next("/login");
  } else {
    next();
  }
}

function loggedIn(to, from, next) {
  const storage = storageValidation();
  if (!store.getters["auth/isLoggedIn"] || !storage) {
    next();
  } else {
    next(from);
  }
}

export default new Router({
  base: process.env.BASE_URL,
  mode: 'history',
  hash: false,
  routes: [
    {
      path: "",
      component: () => import("@/views/layout/LayoutV2.vue"),
      beforeEnter: guardedAuth,
      children: [{
          path: "/",
          name: "home",
          component: () => import("@/views/pages/dashboard/Home.vue"),
        },
      ],
    },
    {
      path: "",
      component: () => import("@/views/layout/LayoutV1.vue"),
      children: [
        {
          path: "/login",
          name: "pagelogin",
          beforeEnter: loggedIn,
          component: () => import("@/views/pages/Login.vue"),
        },
        {
          path: "/register",
          name: "pageRegister",
          beforeEnter: loggedIn,
          component: () => import("@/views/pages/Register.vue"),
        },
        {
          path: "/error-404",
          name: "pageError404",
          component: () => import("@/views/pages/Error404.vue")
        },
      ]
    },
    {
      path: "*",
      redirect: "/error-404"
    }
  ]
});