import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import Helper from "./helper/Helper";
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
Vue.use(Loading);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/fonts.css';
import './assets/scss/main.scss';
import { BootstrapVue } from 'bootstrap-vue'
Vue.use(BootstrapVue)
import Axios from "axios";
Vue.prototype.$http = Axios;
Vue.prototype.$helper = Helper;
Vue.prototype.$http.defaults.baseURL = process.env.VUE_APP_API_ENDPOINT;
Vue.prototype.$http.defaults.headers.post["Content-Type"] = "application/json";

Vue.prototype.$http.interceptors.response.use(
  function(response) {
    return response;
  },
  function(error) {
    if (error.response.status == 401) {
      localStorage.removeItem(process.env.VUE_APP_STORAGE_NAME);
      delete Axios.defaults.headers.common["Authorization"];
      window.location.reload(true);
    }
    return Promise.reject(error);
  }
);

function getLocalStorage() {
  try {
    return Helper.Decrypt(
      // eslint-disable-next-line no-undef
      localStorage.getItem(process.env.VUE_APP_STORAGE_NAME)
    );
  } catch (error) {
    return "";
  }
}

const storage = getLocalStorage();
let token;
if (storage) {
  token = JSON.parse(storage);
}
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] =
    "Bearer " + token.token;
}

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
