/* eslint-disable no-undef */

import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import moduleAuth from "./auth/moduleAuth"

export default new Vuex.Store({
  modules: {
    auth: moduleAuth
  },
  strict: process.env.NODE_ENV !== 'development'
})
