export default {
    isLoggedIn: state => !!state.token,
    isVerified: state => state.verified === "true",
    isConfirmed: state => state.confirmed === "true",
}