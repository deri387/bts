export default {
  REQUEST(state) {
    state.status = "loading";
  },
  LOGIN(state, payload) {
    state.status = "success";
    state.token = payload.token
    state.user = JSON.stringify(payload.user)
    state.verified = JSON.stringify(payload.user.is_verified)
    state.confirmed = JSON.stringify(payload.user.is_confirm)
  },
  LOGOUT(state) {
    state.status = ""
    state.token = ""
    state.user = ""
  },
};